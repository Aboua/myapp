(function() {
    'use strict';

    angular
        .module('myfirstapphipsterApp')
        .factory('authExpiredInterceptor', authExpiredInterceptor);

})();
