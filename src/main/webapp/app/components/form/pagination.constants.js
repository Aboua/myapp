(function() {
    'use strict';

    angular
        .module('myfirstapphipsterApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
